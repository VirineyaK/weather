import UIKit
import CoreLocation

class WeatherMapViewController: UIViewController {
    
    private let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    
    
    var manager = CLLocationManager()
    var coordinatesLatitude = Double()
    var coordinatesLongitude = Double()
    var cityCoordinates = Cities()
    var weatherInCity: WeatherMap?
    
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var currentDayLabel: UILabel! // удаляю дату
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var mainIconImageView: UIImageView!
    @IBOutlet weak var feelsLikeLabel: UILabel!
    
    @IBOutlet weak var currentMinTempLabel: UILabel!
    @IBOutlet weak var currentMaxTempLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var hourlyWeatherCollectionView: UICollectionView!
    @IBOutlet weak var dailyWeatherTableView: UITableView!
    @IBOutlet weak var containerWeatherView: UIView!
    @IBOutlet weak var descriptionWeatherLabel: UILabel!
    @IBOutlet weak var sunriseTimeLabel: UILabel!
    @IBOutlet weak var sunsetTimeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isHidden = true
        self.navigationController?.navigationBar.isTranslucent = true
        
        self.addActivityIndicator()
        self.getLocation()
        self.changeBackground()
    }
    // MARK: - Getting weather by coordinates or city name
    
    @IBAction func searchWeatherInCityButtonPressed(_ sender: UIButton) {
        guard let searchCityController = self.storyboard?.instantiateViewController(withIdentifier: "CityCoordinatesViewController") as? CityCoordinatesViewController else {
            return
        }
        searchCityController.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(searchCityController, animated: true)
        searchCityController.delegate = self
    }
    
    @IBAction func searchСityСoordinatesButtonPressed(_ sender: UIButton) {
        self.getLocation()
    }
    
    // MARK: - Change background depending on time of day
    private func changeBackground() {
        let hour = Calendar.current.component(.hour, from: Date())
        if hour >= 6 && hour <= 19 {
            mainIconImageView.image = UIImage(named: "dayBackgroundImage")
        } else {
            mainIconImageView.image = UIImage(named: "nightBackgroundImage")
        }
        containerWeatherView.layer.cornerRadius = 10
    }
    
    // MARK: - Setting display of current weather
    
    private func openWeather() {
        
        WeatherMapManager.shared.openWeatherInCity(coordinatesLatitude: coordinatesLatitude, coordinatesLongitude: coordinatesLongitude) { [weak self](weatherMap, error) in
            
            self?.temperatureLabel.text = String(Int((weatherMap?.current?.temp ?? 0.0))) + "°"
            self?.cityNameLabel.text = weatherMap?.timezone
            self?.feelsLikeLabel.text = "feels like".localized() + " " + String(Int((weatherMap?.current?.feels_like ?? 0.0))) + "°"
            self?.currentMinTempLabel.text = "min".localized() + " " + String(Int(weatherMap?.daily?.first?.temp?.min ?? 0.0)) + "°"
            self?.currentMaxTempLabel.text = "max".localized() + " " + String(Int(weatherMap?.daily?.first?.temp?.max ?? 0.0)) + "°"
            self?.iconImageView.image = UIImage(named: weatherMap?.current?.weather?[0].icon ?? "")
            self?.descriptionWeatherLabel.text = weatherMap?.current?.weather?.first?.description?.localized() ?? ""
            self?.sunriseTimeLabel.text = self?.formatterTimeOfSunriseAndSunset(time: weatherMap?.current?.sunrise)
            self?.sunsetTimeLabel.text = self?.formatterTimeOfSunriseAndSunset(time: weatherMap?.current?.sunset)
            self?.weatherInCity = weatherMap
            self?.dailyWeatherTableView.reloadData()
            self?.hourlyWeatherCollectionView.reloadData()
        }
    }
    
    private func formatterTimeOfSunriseAndSunset(time: Int?) -> String {
        let date = Date(timeIntervalSince1970: Double(time ?? 0))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH:mm"
        let stringDate = dateFormatter.string(from: date)
        return stringDate
    }
    
    private func addActivityIndicator() {
        view.addSubview(activityIndicator)
        activityIndicator.frame = view.bounds
        activityIndicator.backgroundColor = UIColor(white: 0.0, alpha: 0.97)
        activityIndicator.color = .systemBlue
        activityIndicator.startAnimating()
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(3)) {
            self.activityIndicator.stopAnimating()
        }
    }
    
    // MARK: - Getting coordinates by geolocation
    
    private func getLocation() {
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestAlwaysAuthorization()
        manager.startUpdatingLocation()
    }
}

extension WeatherMapViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location not loaded")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else {return}
        coordinatesLatitude = location.coordinate.latitude
        coordinatesLongitude = location.coordinate.longitude
        self.openWeather()
    }
}

// MARK: - Setting display of hour and day weather

extension WeatherMapViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 24
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = hourlyWeatherCollectionView.dequeueReusableCell(withReuseIdentifier: "HourlyWeatherCollectionViewCell", for: indexPath) as? HourlyWeatherCollectionViewCell else {
            return HourlyWeatherCollectionViewCell()
        }
        
        cell.configureHourlyWeather(hourlyWeather: weatherInCity?.hourly?[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        return CGSize(width: 60, height: 100)
    }
}

extension WeatherMapViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weatherInCity?.daily?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let dailyWeahterCell = tableView.dequeueReusableCell(withIdentifier: "DailyWeatherTableViewCell", for: indexPath) as? DailyWeatherTableViewCell else {
            return DailyWeatherTableViewCell()
        }
        
        dailyWeahterCell.configureDailyWeather(dailyWeather: weatherInCity?.daily?[indexPath.row])
        
        return dailyWeahterCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}

extension WeatherMapViewController: CityCoordinatesControllerDelegate {
    func loadСityСoordinates(_ city: Cities) {
        self.cityCoordinates = city
        self.coordinatesLatitude = cityCoordinates.coord?.lat ?? 0.0
        self.coordinatesLongitude = cityCoordinates.coord?.lon ?? 0.0
        self.openWeather()
    }
}
