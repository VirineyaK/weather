import UIKit

class HourlyWeatherCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var hourLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var tempHourLabel: UILabel!
    
    func configureHourlyWeather(hourlyWeather: HourlyWeather?) {
        let date = Date(timeIntervalSince1970: Double(hourlyWeather?.dt ?? 0))
        let dateFormatter = DateFormatter()
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "HH"
        let stringDate = dateFormatter.string(from: date)
        
        self.hourLabel.text = stringDate
        guard let temp = hourlyWeather?.temp else {return}
        self.tempHourLabel.text = String(Int(temp)) + "°"
        guard let image = hourlyWeather?.weather?.first?.icon else {return}
        self.weatherImageView.image = UIImage(named: image)
        
    }
}
