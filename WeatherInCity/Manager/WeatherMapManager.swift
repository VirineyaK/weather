import Foundation

class WeatherMapManager {
    static let shared = WeatherMapManager()
    private init() {}
    
    func openWeatherInCity(coordinatesLatitude: Double, coordinatesLongitude: Double, complition: @escaping (WeatherMap?, Error?) -> Void) {
        let baseURL = "https://api.openweathermap.org/data/2.5/"
        let appid = "55053a4252cc3ce70b630a8c1d0ba98e"
        let urlString = baseURL + "onecall?lat=\(coordinatesLatitude)&lon=\(coordinatesLongitude)&units=metric&exclude=&appid=\(appid)"
        guard let url = URL(string: urlString) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                guard error == nil,
                    let data = data else { return }
                do {
                    let weatherInCity = try JSONDecoder().decode(WeatherMap.self, from: data)
                    
                    complition(weatherInCity, nil)
                    
                } catch let error {
                    print(error)
                    complition(nil, error)
                }
            }
        }
        task.resume()
    }
}
