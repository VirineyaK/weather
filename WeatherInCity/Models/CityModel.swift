import Foundation
import UIKit

class Cities: Decodable {
    var id: Int?
    var name: String?
    var state: String?
    var country: String?
    var coord: Coordinates?
}

class Coordinates: Decodable {
    var lon: Double?
    var lat: Double?
}
