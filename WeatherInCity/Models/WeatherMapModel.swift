import Foundation

class WeatherMap: Decodable {
    var lat: Double?
    var lon: Double?
    var timezone: String?
    var current: CurrentWeather?
    var hourly: [HourlyWeather]?
    var daily: [DailyWeather]?
}

class CurrentWeather: Decodable {
    var dt: Int?
    var sunrise: Int?
    var sunset: Int?
    var temp: Double?
    var feels_like: Double?
    var pressure: Int?
    var humidity: Int?
    var dew_point: Double?
    var uvi: Double?
    var visibility: Double?
    var clouds: Int?
    var wind_speed: Double?
    var wind_deg: Double?
    var wind_gust: Double?
    var weather: [Weather]?
}

class HourlyWeather: Decodable {
    var dt: Int?
    var temp: Double?
    var feels_like: Double?
    var pressure: Int?
    var humidity: Int?
    var dew_point: Double?
    var clouds: Int?
    var wind_speed: Double?
    var wind_deg: Int?
    var weather: [Weather]?
}

class DailyWeather: Decodable {
    
    var dt: Int?
    var sunrise: Int?
    var sunset: Int?
    var temp: Temperature?
    var wind_speed: Double?
    var wind_deg: Double?
    var weather: [Weather]?
    var clouds:  Double?
    var rain: Double?
    var uvi: Double?
}

class Weather: Decodable {
    var id: Int?
    var main: String?
    var description: String?
    var icon: String?
}

class Temperature: Decodable {
    var day:  Double?
    var min: Double?
    var max: Double?
    var night: Double?
    var eve: Double?
    var morn: Double?
    
}

