protocol CityCoordinatesControllerDelegate: AnyObject {
    func loadСityСoordinates(_ city: Cities)
}

import UIKit

class CityCoordinatesViewController: UIViewController {
    
    weak var delegate: CityCoordinatesControllerDelegate?
    var citiesArray = [Cities]()
    var filteredArrayCities = [Cities]()
    
    let searchController = UISearchController(searchResultsController: nil)
    private var searchBarIsEmpty: Bool {
        guard let text = searchController.searchBar.text else {return false}
        return text.isEmpty
    }
    
    private var isFiltering: Bool {
        let searchBarScopeIsFiltering = searchController.searchBar.selectedScopeButtonIndex != 0
        return searchController.isActive && (!searchBarIsEmpty || searchBarScopeIsFiltering)
    }
    
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainIconImageView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.openJSON()
        self.tableView.reloadData()
        self.changeBackground()
        self.settingSearchController()
    }
    
    // MARK: - Setting search city
    
    private func settingSearchController() {
        
        navigationItem.hidesSearchBarWhenScrolling = true
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search city".localized()
        //                navigationItem.searchController = searchController
        definesPresentationContext = true
        searchContainer.addSubview(searchController.searchBar)
        //        self.navigationController?.isNavigationBarHidden = false
        
        searchController.searchBar.scopeButtonTitles = ["ALL", "BY", "RU", "DE"]
        searchController.searchBar.delegate = self
        navigationController?.navigationBar.isHidden = true
        
        //        searchController.modalPresentationStyle = .formSheet
        searchController.searchBar.barStyle = .black
        //        searchController.searchBar.isTranslucent = true
        //        searchController.searchBar.canse
        
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    // MARK: - Parsing JSON for getting coordinates and city name
    
    private func openJSON() {
        if let path = Bundle.main.path(forResource: "cityList", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                let cities = try JSONDecoder().decode([Cities].self, from: data)
                citiesArray = cities
            } catch {
                
            }
        }
    }
    
    // MARK: - Change background depending on time of day
    
    private func changeBackground() {
        let hour = Calendar.current.component(.hour, from: Date())
        if hour >= 6 && hour <= 19 {
            mainIconImageView.image = UIImage(named: "dayBackgroundImage")
        } else {
            mainIconImageView.image = UIImage(named: "nightBackgroundImage")
        }
    }
}

// MARK - Setting and displaying filtered city

extension CityCoordinatesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredArrayCities.count
        }
        
        return 500
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CityCoordinatesTableViewCell", for: indexPath) as? CityCoordinatesTableViewCell else {
            return CityCoordinatesTableViewCell()
        }
        
        var city: Cities
        if isFiltering {
            city = filteredArrayCities[indexPath.row]
        } else {
            city = citiesArray[indexPath.row]
        }
        cell.textLabel?.text = city.name
        cell.detailTextLabel?.text = city.country
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city: Cities
        if isFiltering {
            city = filteredArrayCities[indexPath.row]
        } else {
            city = citiesArray[indexPath.row]
        }
        self.delegate?.loadСityСoordinates(city)
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension CityCoordinatesViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        let scope = searchBar.scopeButtonTitles![searchBar.selectedScopeButtonIndex]
        filterContentForSearchText((searchController.searchBar.text ?? " "), scope: scope)
    }
    
    private func filterContentForSearchText(_ searchText: String, scope: String = "ALL") {
        filteredArrayCities = citiesArray.filter({ (city: Cities) -> Bool in
            
            let doesCategoryMatch = (scope == "ALL" || (city.country == scope))
            if searchBarIsEmpty {
                return doesCategoryMatch
            }
            return doesCategoryMatch && (city.name?.lowercased().contains(searchText.lowercased()) ?? true)
        })
        tableView.reloadData()
    }
}

extension CityCoordinatesViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text ?? "", scope: searchBar.scopeButtonTitles?[selectedScope] ?? "")
    }
}
