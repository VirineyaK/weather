import UIKit

class DailyWeatherTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weatherImageView: UIImageView!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    
    func configureDailyWeather(dailyWeather: DailyWeather?) {
        let date = Date(timeIntervalSince1970: Double(dailyWeather?.dt ?? 0))
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "EEEE, d MMM"
        let stringDate = dateFormatter.string(from: date)
        
        self.dateLabel.text = stringDate
        guard let weatherIcon = dailyWeather?.weather?[0].icon else {return}
        self.weatherImageView.image = UIImage(named: weatherIcon)
        guard let tempMax = dailyWeather?.temp?.max else {return}
        self.maxTempLabel.text = String(Int(tempMax))
        guard let tempMin = dailyWeather?.temp?.min else {return}
        self.minTempLabel.text = String(Int(tempMin))
    }
    
}
